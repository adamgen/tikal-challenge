#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('module-alias/register');
var express = require("express");
var bodyparser = require("body-parser");
var mongo_1 = require("config/mongo");
var countries_controller_1 = require("resources/countries/countries.controller");
var missions_controller_1 = require("resources/missions/missions.controller");
var cors = require("cors");
mongo_1.connectToDb().then(function () { console.log('connected to db'); });
var server = express();
server.use(cors());
server.use(bodyparser.json({}));
server.use(countries_controller_1.countriesRouter);
server.use(missions_controller_1.missionsRouter);
server.listen(3000);
//   ['', 'user', 'add-expense', 'add-expenses', 'show-expenses'].forEach(route => {
//     server.use('/' + route, express.static('app/dist'));
//   });
//   port = 9000;  
//# sourceMappingURL=api.js.map