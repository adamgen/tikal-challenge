"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getNotify = function () {
    return function (message, message_data) { return (__assign({ message: message, status: 'notify' }, message_data)); };
};
exports.notify = function (string, message_data) {
    return exports.getNotify()(string, message_data);
};
//# sourceMappingURL=notify.js.map