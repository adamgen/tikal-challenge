"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var GenericRouter = /** @class */ (function () {
    function GenericRouter() {
        this.router = express_1.Router();
    }
    GenericRouter.prototype.get = function (path) {
        var handlers = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            handlers[_i - 1] = arguments[_i];
        }
        var _a;
        (_a = this.router).get.apply(_a, [path].concat(handlers));
    };
    GenericRouter.prototype.post = function (path) {
        var handlers = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            handlers[_i - 1] = arguments[_i];
        }
        var _a;
        (_a = this.router).post.apply(_a, [path].concat(handlers));
    };
    return GenericRouter;
}());
exports.GenericRouter = GenericRouter;
//# sourceMappingURL=GenericRouter.js.map