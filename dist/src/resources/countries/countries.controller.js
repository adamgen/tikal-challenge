"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var countries_1 = require("./countries");
var GenericRouter_1 = require("@lib/GenericRouter");
var router = new GenericRouter_1.GenericRouter();
router.get('/countries-by-isolation', countries_1.getCountries);
exports.countriesRouter = router.router;
//# sourceMappingURL=countries.controller.js.map