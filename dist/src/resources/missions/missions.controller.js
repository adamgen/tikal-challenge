"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var missions_1 = require("./missions");
var GenericRouter_1 = require("@lib/GenericRouter");
var router = new GenericRouter_1.GenericRouter();
router.post('/find-closest', missions_1.postMissions);
router.post('/add-dummy-data', missions_1.addDummyData);
exports.missionsRouter = router.router;
//# sourceMappingURL=missions.controller.js.map