"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var missions_schema_1 = require("./missions.schema");
var gmaps_1 = require("@lib/gmaps");
var notify_1 = require("@lib/notify");
var missions_data_1 = require("./missions-data");
exports.setMissionsDataLatLang = function (missionsData) { return __awaiter(_this, void 0, void 0, function () {
    var promises;
    var _this = this;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                promises = [];
                missionsData.forEach(function (mission) { return __awaiter(_this, void 0, void 0, function () {
                    var address, promise;
                    return __generator(this, function (_a) {
                        address = mission.address;
                        promise = gmaps_1.googleMapsClient.geocode({ address: address }).asPromise();
                        promises.push(promise);
                        promise.then(function (data) {
                            mission.location = data.json.results[0].geometry.location;
                        });
                        return [2 /*return*/];
                    });
                }); });
                return [4 /*yield*/, Promise.all(promises)];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
function diff(num1, num2) {
    if (num1 > num2) {
        return (num1 - num2);
    }
    else {
        return (num2 - num1);
    }
}
;
exports.pseudoDistanceBetweenPoints = function (point1, point2) {
    var deltaX = diff(point1.lat, point2.lat);
    var deltaY = diff(point1.lng, point2.lng);
    var dist = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
    return (dist);
};
exports.getClosestAndFurthestMissionsFromLocation = function (missions, location) {
    var farthestMission = {
        distance: 0,
        mission: missions[0],
    };
    var closestMission = {
        distance: Number.MAX_SAFE_INTEGER,
        mission: missions[0],
    };
    missions.forEach(function (mission) {
        var pseudoDistance = exports.pseudoDistanceBetweenPoints(mission.location, location);
        if (pseudoDistance > farthestMission.distance) {
            farthestMission.distance = pseudoDistance;
            farthestMission.mission = mission;
        }
        else if (pseudoDistance < closestMission.distance) {
            closestMission.distance = pseudoDistance;
            closestMission.mission = mission;
        }
    });
    return { farthestMission: farthestMission, closestMission: closestMission };
};
exports.findMissions = function () { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, missions_schema_1.Mission.find({}, { _id: 0, __v: 0 }, ['agent', 'country', 'address', 'date', 'location'])];
    });
}); };
exports.postMissions = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var missions, address, data, _a, farthestMission, closestMission;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0: return [4 /*yield*/, exports.findMissions()];
            case 1:
                missions = _b.sent();
                if (!missions.length) {
                    return [2 /*return*/, res.status(200).json(notify_1.notify('no missions found'))];
                }
                address = req.body.address;
                return [4 /*yield*/, gmaps_1.googleMapsClient.geocode({ address: address }).asPromise()];
            case 2:
                data = _b.sent();
                _a = exports.getClosestAndFurthestMissionsFromLocation(missions, data.json.results[0].geometry.location), farthestMission = _a.farthestMission, closestMission = _a.closestMission;
                // console.log('{ farthestMission, closestMission }', { farthestMission, closestMission });
                return [2 /*return*/, res.json({ farthestMission: farthestMission, closestMission: closestMission })];
        }
    });
}); };
exports.addDummyData = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log('adding dummy');
                return [4 /*yield*/, missions_schema_1.Mission.saveWithLocation(missions_data_1.missionsData)];
            case 1:
                _a.sent();
                return [2 /*return*/, res.status(200).json(true)];
        }
    });
}); };
//# sourceMappingURL=missions.js.map