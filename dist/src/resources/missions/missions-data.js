"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.missionsData = [{
        location: { lat: -22.9865897, lng: -43.1990855 },
        agent: 7,
        country: 'Brazil',
        address: 'Avenida Vieira Souto 168 Ipanema, Rio de Janeiro',
        date: new Date('1995-12-17T19:45:17.000Z')
    },
    {
        location: { lat: 50.0604158, lng: 19.9378657 },
        agent: 5,
        country: 'Poland',
        address: 'Rynek Glowny 12, Krakow',
        date: new Date('2011-04-05T14:05:12.000Z')
    },
    {
        location: { lat: 31.630378, lng: -7.9844423 },
        agent: 7,
        country: 'Morocco',
        address: '27 Derb Lferrane, Marrakech',
        date: new Date('2000-12-31T22:00:00.000Z')
    },
    {
        location: { lat: -23.5481544, lng: -46.6330667 },
        agent: 5,
        country: 'Brazil',
        address: 'Rua Roberto Simonsen 122, Sao Paulo',
        date: new Date('1986-05-05T06:40:23.000Z')
    },
    {
        location: { lat: 50.0619738, lng: 19.9425803 },
        agent: 11,
        country: 'Poland',
        address: 'swietego Tomasza 35, Krakow',
        date: new Date('1997-09-07T16:12:53.000Z')
    },
    {
        location: { lat: 33.6019556, lng: -7.6196086 },
        agent: 3,
        country: 'Morocco',
        address: 'Rue Al-Aidi Ali Al-Maaroufi, Casablanca',
        date: new Date('2012-08-29T07:17:05.000Z')
    },
    {
        location: { lat: -3.3612727, lng: -64.7206394 },
        agent: 8,
        country: 'Brazil',
        address: 'Rua tamoana 418, tefe',
        date: new Date('2005-11-10T11:25:13.000Z')
    },
    {
        location: { lat: 51.24789879999999, lng: 22.5699249 },
        agent: 13,
        country: 'Poland',
        address: 'Zlota 9, Lublin',
        date: new Date('2002-10-17T08:52:19.000Z')
    },
    {
        location: { lat: 35.7886721, lng: -5.8145785 },
        agent: 2,
        country: 'Morocco',
        address: 'Riad Sultan 19, Tangier',
        date: new Date('2017-01-01T15:00:00.000Z')
    },
    {
        location: { lat: 30.4223942, lng: -9.6188086 },
        agent: 9,
        country: 'Morocco',
        address: 'atlas marina beach, agadir',
        date: new Date('2016-12-01T19:21:21.000Z')
    }];
// export const missionsData: IMission[] = [
//     {
//         agent: parseInt('007'),
//         country: 'Brazil',
//         address: 'Avenida Vieira Souto 168 Ipanema, Rio de Janeiro',
//         date: new Date('Dec 17, 1995, 9:45:17 PM'),
//     },
//     {
//         agent: parseInt('005'),
//         country: 'Poland',
//         address: 'Rynek Glowny 12, Krakow',
//         date: new Date('Apr 5, 2011, 5:05:12 PM'),
//     },
//     {
//         agent: parseInt('007'),
//         country: 'Morocco',
//         address: '27 Derb Lferrane, Marrakech',
//         date: new Date('Jan 1, 2001, 12:00:00 AM'),
//     },
//     {
//         agent: parseInt('005'),
//         country: 'Brazil',
//         address: 'Rua Roberto Simonsen 122, Sao Paulo',
//         date: new Date('May 5, 1986, 8:40:23 AM'),
//     },
//     {
//         agent: parseInt('011'),
//         country: 'Poland',
//         address: 'swietego Tomasza 35, Krakow',
//         date: new Date('Sep 7, 1997, 7:12:53 PM'),
//     },
//     {
//         agent: parseInt('003'),
//         country: 'Morocco',
//         address: 'Rue Al-Aidi Ali Al-Maaroufi, Casablanca',
//         date: new Date('Aug 29, 2012, 10:17:05 AM'),
//     },
//     {
//         agent: parseInt('008'),
//         country: 'Brazil',
//         address: 'Rua tamoana 418, tefe',
//         date: new Date('Nov 10, 2005, 1:25:13 PM'),
//     },
//     {
//         agent: parseInt('013'),
//         country: 'Poland',
//         address: 'Zlota 9, Lublin',
//         date: new Date('Oct 17, 2002, 10:52:19 AM'),
//     },
//     {
//         agent: parseInt('002'),
//         country: 'Morocco',
//         address: 'Riad Sultan 19, Tangier',
//         date: new Date('Jan 1, 2017, 5:00:00 PM'),
//     },
//     {
//         agent: parseInt('009'),
//         country: 'Morocco',
//         address: 'atlas marina beach, agadir',
//         date: new Date('Dec 1, 2016, 9:21:21 PM'),
//     }
// ];
//# sourceMappingURL=missions-data.js.map