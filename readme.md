Hi comander, I'm very short on time but the mission is complete. You can look at the code and speak with me directly if you have any questions.

Testing curl commands:
* Run this at the beginning to initiate dummy data`curl -X POST http://localhost:3000/add-dummy-data`
* Run this to get closes and furthest missions `curl -H "Content-Type: application/json" -X POST http://localhost:3000/find-closest -d '{"address": "1600 Amphitheatre Parkway, Mountain View, CA"}'`

Single command to run the app (using bash)
`git clone git@gitlab.com:adamgen/tikal-challenge.git && sh tikal-challenge/run.sh`
