import { INotifyRsponse } from "@interfaces/notifications.interface";
import { IEndPoint } from "@interfaces/endpoint.interface";
import { LatLngLiteral } from "@google/maps";

export interface IMission {
    agent: number;
    country: string;
    address: string;
    date: Date;
    location?: LatLngLiteral;
}

export type TMissionNotifications = 'no missions found';

export type MissionWithDistance = {
    mission: IMission;
    distance: number;
}

export type MissionFurthestClosestResponce = {
    farthestMission: MissionWithDistance;
    closestMission: MissionWithDistance;
}

export interface IMissionEndPoint {
    '/find-closest': IEndPoint<MissionFurthestClosestResponce, INotifyRsponse<TMissionNotifications>>
    '/add-dummy-data': IEndPoint<boolean, INotifyRsponse<TMissionNotifications>>
}
