import { IEndPoint } from "@interfaces/endpoint.interface";
import { INotifyRsponse } from "@interfaces/notifications.interface";

export type TCountryNotifications = 'a';

export interface ICountryEndpoint {
    '/countries-by-isolation': IEndPoint<string[], INotifyRsponse<TCountryNotifications>>;
}
