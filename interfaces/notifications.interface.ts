export interface INotifyRsponse<Tmessage extends string = string> {
    message?: Tmessage;
    success?: boolean;
    status: 'notify';
}
