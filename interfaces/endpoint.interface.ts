export interface IEndPoint<success = any, fail = any> {
    success: success;
    fail: fail;
    all: fail | success;
};
