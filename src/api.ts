#!/usr/bin/env node
require('module-alias/register');
import express = require('express');

import bodyparser = require('body-parser');
import { connectToDb } from 'config/mongo';
import { countriesRouter } from 'resources/countries/countries.controller';
import { missionsRouter } from 'resources/missions/missions.controller';
import cors = require('cors');

connectToDb().then(() => { console.log('connected to db') });

const server = express();
server.use(cors());

server.use(bodyparser.json({}));

server.use(countriesRouter);
server.use(missionsRouter);

server.listen(3000);

//   ['', 'user', 'add-expense', 'add-expenses', 'show-expenses'].forEach(route => {
//     server.use('/' + route, express.static('app/dist'));
//   });
//   port = 9000;  