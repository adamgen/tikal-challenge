import { TypedRequestHandler } from '@lib/GenericRouter';
import { ICountryEndpoint } from '@interfaces/country.interface';
import { Mission, IMissionDocument } from '../missions/missions.schema';
import * as _ from 'lodash'
import { IMission } from '@interfaces/mission.interface';
import { findMissions } from '../missions/missions';

// import { notify } from '@lib/notify';
interface IAgentIsolationStatus {
    [agent: number]: boolean;
}

const getIsolatedAgents = (missions: IMission[]): IAgentIsolationStatus => {
    const agents: IAgentIsolationStatus
        = {};

    _.each(missions, mission => {
        const { agent } = mission;
        if (agents[agent] === true) {
            agents[agent] = false;
        } else if (agents[agent] === undefined) {
            agents[agent] = true;
        }
    });

    return agents;
}

const getMostIsolatedCountry = (missions: IMission[]) => {
    const agents = getIsolatedAgents(missions);

    const countries: { [country: string]: number }
        = {};
    let largestIsolation = {
        country: '',
        count: 0,
    };

    missions.forEach(mission => {
        const { agent, country } = mission;
        if (!agents[agent]) {
            return;
        }
        countries[country] = _.get(countries, country, 0) + 1;
        if (countries[country] <= largestIsolation.count) {
            return;
        }
        largestIsolation.count = countries[country];
        largestIsolation.country = country;
    });
    return largestIsolation;
}

export const getCountries: TypedRequestHandler<ICountryEndpoint['/countries-by-isolation']> = async (req, res) => {
    let missions = await findMissions();
    if (missions.length < 1) {
        return res.status(200).json([]);
    }

    const largestIsolation = getMostIsolatedCountry(missions);
    return res.status(200).json([largestIsolation.country]);
}
