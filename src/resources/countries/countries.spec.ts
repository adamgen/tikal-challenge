import 'module-alias/register';
import 'jasmine-sinon';

import * as _ from 'lodash'
import { mockReq, mockRes } from 'sinon-express-mock';
import { mockDb, resetDb } from '@lib/test';
import { missionsData } from '../missions/missions-data';
import { getCountries } from './countries';
import { getNotify } from '@lib/notify';
import { TCountryNotifications } from '@interfaces/country.interface';
import { Mission } from '../missions/missions.schema';
// import { ICountryEndpoint } from '@interfaces/country.interface';

// const notify = getNotify<TCountryNotifications>();

const addSpecDataToDb = () => {
    Mission.insertMany(missionsData);
}

mockDb();
mockDb();

describe('countries using GET when thers no data in the db', async () => {
    beforeEach(resetDb);

    it('returns an empty array', async (done) => {
        const getReq = mockReq();
        const getRes = mockRes();

        await getCountries(getReq, getRes, _.noop);
        expect(getRes.json).toHaveBeenCalledWith([]);

        done();
    });

    it('returns an empty array', async (done) => {
        const getReq = mockReq();
        const getRes = mockRes();

        await getCountries(getReq, getRes, _.noop);
        expect(getRes.json).toHaveBeenCalledWith([]);

        done();
    });
});

describe('countries using GET when there is data in the db', async () => {
    beforeEach(addSpecDataToDb);
    afterEach(resetDb);

    it('returns an array with single value of largest isolation - moroco', async (done) => {
        const getReq = mockReq();
        const getRes = mockRes();
        await getCountries(getReq, getRes, _.noop);
        expect(getRes.json).toHaveBeenCalledWith(['Morocco']);

        done();
    });
});
