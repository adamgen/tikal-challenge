import { getCountries } from './countries';
import { GenericRouter } from '@lib/GenericRouter'
import { ICountryEndpoint } from '@interfaces/country.interface';

const router = new GenericRouter<ICountryEndpoint>();

router.get<ICountryEndpoint['/countries-by-isolation']>('/countries-by-isolation', getCountries);

export const countriesRouter = router.router;
