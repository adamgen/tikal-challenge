import { postMissions, addDummyData } from './missions';
import { GenericRouter } from '@lib/GenericRouter'
import { IMissionEndPoint } from '@interfaces/mission.interface';

const router = new GenericRouter<IMissionEndPoint>();

router.post<IMissionEndPoint['/find-closest']>('/find-closest', postMissions);

router.post<IMissionEndPoint['/add-dummy-data']>('/add-dummy-data', addDummyData);

export const missionsRouter = router.router;
