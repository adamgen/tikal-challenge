import { TypedRequestHandler } from "@lib/GenericRouter";
import { IMissionEndPoint, IMission, MissionWithDistance } from "@interfaces/mission.interface";
import { Mission, IMissionDocument } from "./missions.schema";
import * as _ from 'lodash'
import { GeocodingResponse, ClientResponse } from "@google/maps";
import { googleMapsClient } from "@lib/gmaps";
import { notify } from "@lib/notify";
import { missionsData } from "./missions-data";

type GooleResponcePromise = Promise<ClientResponse<GeocodingResponse>>;

type LatLng = {
    lat: number;
    lng: number;
}

export const setMissionsDataLatLang = async (missionsData: IMission[]) => {
    const promises: GooleResponcePromise[] = [];
    missionsData.forEach(async mission => {
        const { address } = mission;
        const promise = googleMapsClient.geocode({ address }).asPromise();
        promises.push(promise);
        promise.then((data) => {
            mission.location = data.json.results[0].geometry.location;
        });
    });
    return await Promise.all(promises);
}

function diff(num1: number, num2: number) {
    if (num1 > num2) {
        return (num1 - num2);
    } else {
        return (num2 - num1);
    }
};

export const pseudoDistanceBetweenPoints = (point1: LatLng, point2: LatLng) => {
    var deltaX = diff(point1.lat, point2.lat);
    var deltaY = diff(point1.lng, point2.lng);
    var dist = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
    return (dist);
};

export const getClosestAndFurthestMissionsFromLocation = (missions: IMission[], location: LatLng) => {
    const farthestMission: MissionWithDistance = {
        distance: 0,
        mission: missions[0],
    };
    const closestMission: MissionWithDistance = {
        distance: Number.MAX_SAFE_INTEGER,
        mission: missions[0],
    };

    missions.forEach(mission => {
        const pseudoDistance = pseudoDistanceBetweenPoints(mission.location, location);
        if (pseudoDistance > farthestMission.distance) {
            farthestMission.distance = pseudoDistance;
            farthestMission.mission = mission;
        } else if (pseudoDistance < closestMission.distance) {
            closestMission.distance = pseudoDistance;
            closestMission.mission = mission;
        }
    });

    return { farthestMission, closestMission };
}

export const findMissions = async (): Promise<IMissionDocument[]> => {
    return Mission.find(
        {},
        { _id: 0, __v: 0 },
        ['agent', 'country', 'address', 'date', 'location'],
    );
}

export const postMissions: TypedRequestHandler<IMissionEndPoint['/find-closest']> = async (req, res) => {
    const missions = await findMissions();

    if (!missions.length) {
        return res.status(200).json(notify('no missions found'));
    }

    const { address } = req.body;
    const data = await googleMapsClient.geocode({ address }).asPromise();

    const { farthestMission, closestMission } = getClosestAndFurthestMissionsFromLocation(missions, data.json.results[0].geometry.location);
    // console.log('{ farthestMission, closestMission }', { farthestMission, closestMission });

    return res.json({ farthestMission, closestMission });
}

export const addDummyData: TypedRequestHandler<IMissionEndPoint['/add-dummy-data']> = async (req, res) => {
    console.log('adding dummy');

    await Mission.saveWithLocation(missionsData);
    return res.status(200).json(true);
}
