import { postMissions, setMissionsDataLatLang } from './missions';
import { resetDb, mockDb } from '@lib/test';
import { mockReq, mockRes } from 'sinon-express-mock';
import * as _ from 'lodash'
import { Mission } from './missions.schema';
import { missionsData } from './missions-data';
import { notify } from '@lib/notify';

const addSpecDataToDb = async () => {
    // this will get the location coordinates for all missions before the insert
    // await Mission.saveWithLocation(missionsData);
    await Mission.insertMany(missionsData);
}

mockDb();

describe('missionsData gathering from google api', async () => {
    it('get location from google with success', async () => {
        await setMissionsDataLatLang(missionsData);
        missionsData.forEach(mission => {
            expect(_.keys(mission.location)).toEqual(['lat', 'lng']);
        });
    });
});

describe('missions connection to google api', async () => {
    beforeEach(addSpecDataToDb);
    afterEach(resetDb);

    it('', async (done) => {
        const postReq = mockReq({ body: { address: '1600 Amphitheatre Parkway, Mountain View, CA' } });
        const postRes = mockRes();

        await postMissions(postReq, postRes, _.noop);

        expect(postRes.json).not.toHaveBeenCalledWith(notify('no missions found'));

        done();
    });
}); 
