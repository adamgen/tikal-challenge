import { Document, Model, model, Schema } from 'mongoose';
import { IMission } from '@interfaces/mission.interface';
import { setMissionsDataLatLang } from './missions';

export interface IMissionDocument extends Document, IMission { }

export interface IMissionModel extends Model<IMissionDocument> {
  saveWithLocation(missions: IMission[]): void;
}

const MissionSchema: Schema = new Schema({
  agent: Number,
  country: String,
  address: String,
  date: Date,
  location: {
    lat: Number,
    lng: Number,
  },
});

MissionSchema.static('saveWithLocation', async function (missions: IMission[]) {
  await setMissionsDataLatLang(missions);
  await Mission.insertMany(missions);
});

export const Mission: IMissionModel = model<IMissionDocument, IMissionModel>('Mission', MissionSchema);
