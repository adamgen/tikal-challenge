import _ = require('lodash');
import mongoose = require('mongoose');

const getConnectionString = () => 'mongodb://localhost/missions';

export const connectToDb = async () => {
  mongoose.connect(getConnectionString(), { useNewUrlParser: true });
  mongoose.connection.on('error', (err) => {
    console.log('err', err);
  });
  mongoose.set('debug', true);
  return new Promise((resolve, reject) => {
    mongoose.connection.once('open', resolve);
  });
};
