import mongoose = require('mongoose');
import { Mockgoose } from 'mockgoose';
import { connectToDb } from 'config/mongo';

// see https://github.com/Mockgoose/Mockgoose/issues/22
Mockgoose.prototype.prepareStorage = function () {
    const _this = this;
    return new Promise(function (resolve, reject) {
        Promise.all([_this.getTempDBPath(), _this.getOpenPort()]).then(promiseValues => {
            const dbPath = promiseValues[0];
            const openPort = promiseValues[1].toString();
            const storageEngine = _this.getMemoryStorageName();
            const mongodArgs = ['--port', openPort, '--storageEngine', storageEngine, '--dbpath', dbPath];
            _this.mongodHelper.mongoBin.commandArguments = mongodArgs;
            const mockConnection = () => {
                _this.mockConnectCalls(_this.getMockConnectionString(openPort));
                resolve();
            };
            _this.mongodHelper.run().then(mockConnection).catch(mockConnection);
        });
    });
};

const mock = new Mockgoose(mongoose);

export const mockDb = async () => {
    await mock.prepareStorage();
    connectToDb();
}

export const resetDb = async () => {
    await mock.helper.reset();
};

// export class MockDB {
//     mock = new Mockgoose(mongoose);
//     mockDb = async () => {
//         await this.mock.prepareStorage();
//         connectToDb();
//     }
//     resetDb = async () => {
//         await this.mock.helper.reset();
//     }
// }