import { INotifyRsponse } from '@interfaces/notifications.interface';

export const getNotify = <TMessage extends string>() => {
    return (message: TMessage, message_data?: object): INotifyRsponse<TMessage> => ({
        message,
        status: 'notify',
        ...message_data,
    });
}

export const notify = <TMessage extends string>(string: TMessage, message_data?: object) =>
    getNotify<TMessage>()(string, message_data);
