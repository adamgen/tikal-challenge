import { Request, Response, Router } from 'express';
import { NextFunction } from 'express-serve-static-core';
import { IEndPoint } from '@interfaces/endpoint.interface';

// https://www.restapitutorial.com/httpstatuscodes.html
type HttpStatusCodes = 400 | 422 | 404 | 500 | 200;

export interface TypedResponse<TJsonBody> extends Response {
  status(code: HttpStatusCodes): this;

  json(body?: TJsonBody): this;
}

export type TypedRequestHandler<TJsonBody extends IEndPoint> = (req: Request, res: TypedResponse<TJsonBody['all']>, next: NextFunction) => any;

export class GenericRouter<TEndpoints> {
  router: Router;

  constructor() {
    this.router = Router();
  }

  get<TJsonBody extends IEndPoint>(path: Extract<keyof TEndpoints, string>, ...handlers: TypedRequestHandler<TJsonBody['all']>[]) {
    this.router.get(path, ...handlers);
  }

  post<TJsonBody extends IEndPoint>(path: Extract<keyof TEndpoints, string>, ...handlers: TypedRequestHandler<TJsonBody['all']>[]) {
    this.router.post(path, ...handlers);
  }
}
