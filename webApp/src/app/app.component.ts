import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  address = '1600 Amphitheatre Parkway, Mountain View, CA';

  rawResult: any = {};
  constructor(
    private apiService: ApiService,
  ) { }

  ngOnInit() {
    this.findClosest();
  }

  changeAddress(event) {
    this.address = event.target.value;
    this.findClosest();
  }

  findClosest() {
    this.apiService.findClosest(this.address).subscribe(rawResult => this.rawResult = rawResult);
  }

  initData() {
    this.apiService.initData().subscribe(a => this.rawResult = 'ready to roll, type you home address in the box below');
  }
}
