import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {
  baseUrl = 'http://localhost:3000/';
  constructor(private httpClient: HttpClient) { }

  initData() {
    return this.httpClient.post(this.baseUrl + 'add-dummy-data', true);
  }

  findClosest(address) {
    return this.httpClient.post(this.baseUrl + 'find-closest', { address });
  }
}
